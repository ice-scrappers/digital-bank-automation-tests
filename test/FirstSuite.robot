
Library ConnectAllRobotListener.ConnectAllListener configFile=../connectallConfig.toml

*** Test Case ***
Test Case 1
    [Tags]    caseId=814544019575    setId=814544020835    type=Functional    name="Test Case 1"    description="Test case 1 long description"     
    Should Be Equal    framework    framework

Test Case 2
    [Tags]    caseId=814544020025    setId=814544020835    type=Regression     name="Test Case 2"    description="Test case 2 long description" 
    Should Be Equal    Robot    Robot
    Should Be Equal    Robot    Robot
    Should Be Equal    Robot    Robot

Test Case 3
    [Tags]    caseId=814544019113    setId=814544020835  type=Functional     name="Test Case 3"    description="Test case 3 long description" 
    Should Be True    1<2

Test Case 4
    [Tags]    caseId=814544018615    setId=814544020835  type=Functional     name="Test Case 3"    description="Test case 4 long description" 
    Should Be True    1<2

Test Case 5
    [Tags]    caseId=814544018017    setId=814544020835  type=Functional     name="Test Case 3"    description="Test case 5 long description" 
    Should Be True    1<2

