*** Test Case ***
Test Case 4
     [Tags]    name="TC166"     type=Functional     description="Test case 1 long description"    caseId=https://rally1.rallydev.com/slm/webservice/v2.0/testcase/725252203039    setId=https://rally1.rallydev.com/slm/webservice/v2.0/testset/725274094505     
   Should Be Equal    Robot    Robot
    Sleep   1s

Test Case 5
    [Tags]    name="TC165"     type=Functional     description="Test case 1 long description"    caseId=https://rally1.rallydev.com/slm/webservice/v2.0/testcase/725252356973    setId=https://rally1.rallydev.com/slm/webservice/v2.0/testset/725274094505     
    Should Be Equal    Robot    Robot
    Sleep   1s

Test Case 6 This is Big One With Lengthy Name Check Table Wrap up
    [Tags]    name="TC168"     type=Functional     description="Test case 1 long description"    caseId=https://rally1.rallydev.com/slm/webservice/v2.0/testcase/725252657901    setId=https://rally1.rallydev.com/slm/webservice/v2.0/testset/725274094505     
    Should Be True    1>2
    Sleep   10s

Test Case 7
    [Tags]    caseId=814544016845    type=Functional     name="Test Case 3"    description="Test case 6 long description" 
    Should Be True    1<2